package com.gost;

public class Factorial {

    public static void count(int m, int r) {
        long result = factorial(m) / factorial(r) * factorial(m - r);
        System.out.println("Factorial result is: " + result + "\n\n");
    }

    /**
     *   Factorial using recursion
     */
    private static long factorial(int n){
        if (n == 0)
            return 1;
        else
            return (n * factorial(n-1));
    }

    /**
     *   Factorial using loop
     */
    private static long factorial1(int n) {
        long result = 1;

        for (int i = 2; i <= n; i++) {
            result = result * i;
        }

        return result;
    }
}
