package com.gost;

public class TwoSeven {

    public static void print() {

        StringBuilder str = new StringBuilder();

        for(int i = 1; i <= 100; i++) {

            if(i % 2 == 0){
                str.append("Two");
            }

            if(i % 7 == 0){
                str.append("Seven");
            }

            if(str.length() == 0) {
                str.append(i);
            }

            System.out.println(str);

            str.setLength(0);

        }

        System.out.println("\n\n");
    }
}
