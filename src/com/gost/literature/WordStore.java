package com.gost.literature;

import java.util.*;

public class WordStore {

    public static void countWords() {
        HashMap<String, WordEntity> store = new HashMap<>();
        String[] words = getWords();

        for(String word : words) {

            if(store.containsKey(word)) {
                WordEntity entity = store.get(word);
                entity.setAmount(entity.getAmount() + 1);
            } else {
                WordEntity entity = new WordEntity(word, 1);
                store.put(word, entity);
            }

        }

        printWords(store);
    }


    private static String[] getWords() {
        String text = "Карл у Клары украл кораллы а Клара у Карла украла кларнет. Королева Клара кавалера Карла строго карала за кражу кораллов";
        return text.replaceAll("\\.", "").split(" ");
    }


    private static void printWords(HashMap<String, WordEntity> store) {
        List<WordEntity> values = new ArrayList<>(store.values());
        values.sort((w1, w2) -> w2.getAmount() - w1.getAmount());
        values.forEach(System.out::println);
    }

}
